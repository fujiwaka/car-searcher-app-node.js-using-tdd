var express = require('express');
var request = require('request');
var async = require('async');
var router = express.Router();
var obfuscate = require('../test/obfsucate');


//var uri = "http://vcache.arnoldclark.com/imageserver/AVRTNKC5M5-DUM5/";



/* GET home page. */
router.get('/', function(req, res) {
  res.render('carSearcher');
});

/*POST to search */
router.post('/', function(req, res) {
  if (req.body.registrationplate && req.body.stocknumber) {
    var availableImages = [];
   var uri = "http://vcache.arnoldclark.com/imageserver/"+obfuscate(req.body.stocknumber,req.body.registrationplate);
   var images = [uri+"/350/f",uri+"/350/i",uri+"/350/r",uri+"/350/4",uri+"/350/5",uri+"/350/6" ];
    // request(uri, function (error, response, body) {
    //   if (!error && response.statusCode == 200) {
    //     res.render('carSearcher', {location: uri });
    //   }
    // });
    async.eachSeries(images, function(item, mycallback){
      request(item, function (error, response, body) {
        console.log(uri);
        console.log(item);
        if (!error && response.statusCode == 200) {
          request('http://vcache.arnoldclark.com/imageserver/', function (error, response, secondBody) {
            if (!error && response.statusCode == 200) {
              if (body!==secondBody) {
                availableImages.push(item)
                mycallback();
              }
              else{mycallback();}
              }
            })
          }
        })
      },
      function(err){
        if(err){}
        else {
          console.log("No Errors");
          console.log(availableImages);
          if (availableImages.length === 0) {
            res.render('carSearcher', {nophotos: "No photos available with these details."})
          }
          res.render('carSearcher', {photos: availableImages, info: req.body.stocknumber});
        }
      });
  }
  else {
    res.redirect('/search');
  }
});


module.exports = router;
